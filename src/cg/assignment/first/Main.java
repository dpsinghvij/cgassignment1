package cg.assignment.first;

import cg.assignment.first.library.*;
import cg.assignment.first.utils.Utility;

import java.util.Scanner;

public class Main {



    public static void main(String[] args) {
	// write your code here
        Scanner scan = new Scanner(System.in);
        Utility.print("Please enter the question number you want to test?\n2.\n3.");
        int input = scan.nextInt();
        if(input ==2){
            showQuestion2();
        }else if(input==3){
            showQuestion3();
        }

    //    test(scan);

    }


    private static void showQuestion2() {
        Vector v1= Utility.takeVectorAsInput("1");
        Vector v2= Utility.takeVectorAsInput("2");
        // Ques 2 answer
        Utility.print("Rotation matrix for given input is -");
        RotationMatrixComp rotationMatrixComp= new RotationMatrixComp(v1,v2);
        System.out.println(rotationMatrixComp);
    }

    private static void showQuestion3() {

        // Ques 3 Part c
        // Mwc and Mcw
        Point vrp= Utility.takePointAsInput("VRP");
        Vector vpn= Utility.takeVectorAsInput("VPN");
        Vector vup= Utility.takeVectorAsInput("VUP");
        // initialize transformation Matrix for vector VPN,VUP and point VRP
        TransformationMatrix transformationMatrix= new TransformationMatrix(vpn,vup,vrp);
        Utility.print("World to camera Transformation Matrix is ");
        // TransformationMatrix world to camera
        float[][] worldToCamera = transformationMatrix.getWorldToCamera();
        MatrixHelper.displayMatrix(worldToCamera);
        Utility.print("Camera to World Transformation Matrix is ");
        // Transformation Matrix camera to World
        float[][] cameraToWorld = transformationMatrix.getCameraToWorld();
        MatrixHelper.displayMatrix(cameraToWorld);

        // Ques 3 part D
        //Mwl and Mlw
        Point lrp= Utility.takePointAsInput("LRP");
        Vector lpn= Utility.takeVectorAsInput("LPN");
        Vector lup= Utility.takeVectorAsInput("LUP");
        // initialize transformation Matrix for vector LPN,LUP and point LRP
        TransformationMatrix lightTransformationMatrix= new TransformationMatrix(lpn,lup,lrp);
        Utility.print("World to Light Transformation Matrix is ");
        // Transformation matrix world to light
        float[][] worldToLight = lightTransformationMatrix.getWorldToLight();
        MatrixHelper.displayMatrix(worldToLight);
        Utility.print("Camera to World Transformation Matrix is ");
        // Transformation matrix light to world
        float[][] lightToWorld = lightTransformationMatrix.getLightToWorld();
        MatrixHelper.displayMatrix(lightToWorld);

        // Ques 3 part e.
        try {
            //Transformation Matrix Camera to light
            // Mcl= Multiplication of Transformation matrix worldToLight and cameraToWorld
            // Mcl = Mwl*Mcw
            float Mcl[][]= MatrixHelper.multiply(worldToLight,cameraToWorld);
            Utility.print("Camera to Light Transformation Matrix is ");
            MatrixHelper.displayMatrix(Mcl);

            //Transformation Matrix Camera to light
            //Mlc= Multiplication of Transformation matrix worldToCamera and lightToWorld
            // Mlc =  Mwc*Mlw
            float Mlc[][]= MatrixHelper.multiply(worldToCamera,lightToWorld);
            Utility.print("Camera to Light Transformation Matrix is ");
            MatrixHelper.displayMatrix(Mcl);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

  /*  private static void showWorldCameraTransform() {
        Point vrp= Utility.takePointAsInput("VRP");
        Vector vpn= Utility.takeVectorAsInput("VPN");
        Vector vup= Utility.takeVectorAsInput("VUP");
        TransformationMatrix transformationMatrix= new TransformationMatrix(vpn,vup,vrp);
        Utility.print("World to camera Transformation Matrix is ");
        float[][] worldToCamera = transformationMatrix.getWorldToPoint();
        MatrixHelper.displayMatrix(worldToCamera);
        Utility.print("Camera to World Transformation Matrix is ");
        float[][] cameraToWorld = transformationMatrix.getPointToWorld();
        MatrixHelper.displayMatrix(cameraToWorld);
    }

    private static void showWorldLightTransform() {
        Point lrp= Utility.takePointAsInput("LRP");
        Vector lpn= Utility.takeVectorAsInput("LPN");
        Vector lup= Utility.takeVectorAsInput("LUP");
        TransformationMatrix transformationMatrix= new TransformationMatrix(lpn,lup,lrp);
        Utility.print("World to Light Transformation Matrix is ");
        float[][] worldToLight = transformationMatrix.getWorldToPoint();
        MatrixHelper.displayMatrix(worldToLight);
        Utility.print("Camera to World Transformation Matrix is ");
        float[][] lightToWorld = transformationMatrix.getPointToWorld();
        MatrixHelper.displayMatrix(lightToWorld);
    }


    private static void test() {
        Vector v1= new Vector(5,5,5);
        Vector v2= new Vector(5,8,9);
        // Ques 2 answer
        RotationMatrixComp rotationMatrixComp= new RotationMatrixComp(v1,v2);
        //     System.out.println(rotationMatrixComp);
        // Ques 3
        // part c)
        Point vrp= new Point(5,6,3);
        Vector vpn= new Vector(-2,-2,-1);
        Vector vup= new Vector(-2,1,1);
        // M= R*T
        TransformationMatrix transformationMatrix= new TransformationMatrix(vpn,vup,vrp);
        MatrixHelper.displayMatrix(transformationMatrix.getWorldToPoint());
        MatrixHelper.displayMatrix(transformationMatrix.getPointToWorld());
        *//*float[][] rotationMatrix = rotationMatrixComp.getRotationMatrix();
        MatrixHelper.displayMatrix(rotationMatrix);
        float[][] inverseRotationMatrix = rotationMatrixComp.getInverseRotationMatrix();
        MatrixHelper.displayMatrix(inverseRotationMatrix);*//*
       *//* Point point= new Point(5,5,5);
        float[][] inverseTranslationMatrix = point.getTranslationMatrix(true);
        float[][] translationMatrix = point.getTranslationMatrix(false);
        try {
            MatrixHelper.displayMatrix(MatrixHelper.multiply(translationMatrix,inverseTranslationMatrix));
        } catch (Exception e) {
            e.printStackTrace();
        }*//*
    }*/
}
