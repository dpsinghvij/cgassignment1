package cg.assignment.first.utils;

import cg.assignment.first.library.Point;
import cg.assignment.first.library.Vector;

import java.util.Scanner;

/**
 * Created by davinder on 3/10/16.
 */
public class Utility {

    public static void print(String message){
        System.out.println(message);
    }

    /**
     * Helper method to input values of vector elements
     * @param message name of vector
     * @return
     */
    public static Vector takeVectorAsInput(String message){
        Scanner scanner= new Scanner(System.in);
        Vector vector= new Vector();
        print("Please enter x value of vector "+message);
        vector.setX(scanner.nextFloat());
        print("Please enter y value of vector "+message);
        vector.setY(scanner.nextFloat());
        print("Please enter z value of vector "+message);
        vector.setZ(scanner.nextFloat());
        return vector;
    }


    /**
     * Helper method to input values of point elements
     * @param message name of Point
     * @return
     */
    public static Point takePointAsInput(String message){
        Scanner scanner= new Scanner(System.in);
        Point point= new Point();
        print("Please enter x value of point "+message);
        point.setX(scanner.nextFloat());
        print("Please enter y value of point "+message);
        point.setY(scanner.nextFloat());
        print("Please enter z value of point "+message);
        point.setZ(scanner.nextFloat());
        return point;
    }

    public static String fmt(float value) {
        return String.format("%6.3f",value);
    }
}
