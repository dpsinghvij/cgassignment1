package cg.assignment.first.library;

/**
 * Created by davinder on 2/10/16.
 */
public class Point {

    private float x;
    private float y;
    private float z;

    public Point(){

    }

    public Point(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Computes 4x4 matrix of the point
     * @param isInverse if true- inverse of translation matrix is returned
     * @return
     */
    public float[][] getTranslationMatrix(boolean isInverse){
        // A multplier is created using isInverse
        // if isInverse is true then multiplier is 1 otherwise it is -1
        int multplier=isInverse?1:-1;
        float[][] translationMatrix = new float[4][];
        translationMatrix[0]= new float[]{1f,0f,0f,multplier*x};
        translationMatrix[1]= new float[]{0f,1f,0f,multplier*y};
        translationMatrix[2]= new float[]{0f,0f,1f,multplier*z};
        translationMatrix[3]=new float[]{0f,0f,0f,1f};
        return translationMatrix;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }
}
